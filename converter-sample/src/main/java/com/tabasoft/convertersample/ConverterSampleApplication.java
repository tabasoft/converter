package com.tabasoft.convertersample;

import com.tabasoft.converter.configuration.ExtendedConversionServiceConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Configuration
@Import(ExtendedConversionServiceConfig.class)
public class ConverterSampleApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConverterSampleApplication.class, args);
    }
}