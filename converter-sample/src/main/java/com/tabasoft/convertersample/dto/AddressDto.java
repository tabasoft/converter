package com.tabasoft.convertersample.dto;

public class AddressDto {
    private String city;
    private String street;
    private Long houseNo;

    public Long getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(Long houseNo) {
        this.houseNo = houseNo;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
