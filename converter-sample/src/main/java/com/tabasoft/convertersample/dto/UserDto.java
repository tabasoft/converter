package com.tabasoft.convertersample.dto;

import java.util.List;

public class UserDto
{
    private String name;
    private AddressDto address;
    private List<AddressDto> parentAddresses;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public AddressDto getAddress()
    {
        return address;
    }

    public void setAddress(AddressDto address)
    {
        this.address = address;
    }

    public List<AddressDto> getParentAddresses()
    {
        return parentAddresses;
    }

    public void setParentAddresses(List<AddressDto> parentAddresses)
    {
        this.parentAddresses = parentAddresses;
    }
}
