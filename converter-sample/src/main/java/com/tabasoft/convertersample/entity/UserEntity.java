package com.tabasoft.convertersample.entity;

import com.tabasoft.convertersample.dto.AddressDto;

import java.util.List;

public class UserEntity
{
    private String name;
    private AddressEntity address;
    private List<AddressEntity> parentAddresses;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public AddressEntity getAddress()
    {
        return address;
    }

    public void setAddress(AddressEntity address)
    {
        this.address = address;
    }

    public List<AddressEntity> getParentAddresses()
    {
        return parentAddresses;
    }

    public void setParentAddresses(List<AddressEntity> parentAddresses)
    {
        this.parentAddresses = parentAddresses;
    }
}
