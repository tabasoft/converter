package com.tabasoft.convertersample;

import com.tabasoft.converter.api.ExtendedConversionService;
import com.tabasoft.convertersample.dto.AddressDto;
import com.tabasoft.convertersample.dto.UserDto;
import com.tabasoft.convertersample.entity.AddressEntity;
import com.tabasoft.convertersample.entity.UserEntity;
import com.tabasoft.convertersample.testdata.TestCanConvertBooleanSource;
import com.tabasoft.convertersample.testdata.TestCanConvertBooleanTarget;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.util.collections.ListUtil;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConverterSampleApplicationTests {

	public static final String TEST_STREET_NAME = "Test Street";
	public static final long TEST_HOUSE_NO = 1L;
	public static final String TEST_CITY_NAME = "Test City";
    public static final String TEST_USER_NAME = "TEST USER NAME";
    @Resource(name = ExtendedConversionService.BEAN_NAME)
	private ExtendedConversionService extendedConversionService;

	@Test
	public void contextLoads() {
	}

	@Test
	public void ExtendedConversionServiceBeanExists()
	{
		Assert.assertNotNull(extendedConversionService);
	}

	@Test
	public void canConvertAddressEntityToDto()
	{
        AddressEntity addressEntity = buildAddressEntity();
		AddressDto addressDto = extendedConversionService.convert(addressEntity, AddressDto.class);
		Assert.assertNotNull(addressDto);
		Assert.assertEquals(addressDto.getCity(), TEST_CITY_NAME);
	}

	@Test
    public void canConvertBooleanField()
    {
        TestCanConvertBooleanSource source = new TestCanConvertBooleanSource();
        source.setTest(true);
        TestCanConvertBooleanTarget target = extendedConversionService.convert(source, TestCanConvertBooleanTarget.class);
        Assert.assertNotNull(target);
        Assert.assertNotNull(target.isTest());
        Assert.assertEquals(target.isTest(), true);
    }

    @Test
    public void canConvertInnerClass()
    {
        AddressEntity addressEntity = buildAddressEntity();
        UserEntity userEntity = new UserEntity();
        userEntity.setName(TEST_USER_NAME);
        userEntity.setAddress(addressEntity);
        List<AddressEntity> addresses = new ArrayList<>();
        addresses.add(buildAddressEntity());
        addresses.add(buildAddressEntity());
        userEntity.setParentAddresses(addresses);
        UserDto userDto = extendedConversionService.convert(userEntity, UserDto.class);
        Assert.assertNotNull(userDto);
    }

    private AddressEntity buildAddressEntity()
    {
        AddressEntity addressEntity = new AddressEntity();
        addressEntity.setHouseNo(TEST_HOUSE_NO);
        addressEntity.setStreet(TEST_STREET_NAME);
        addressEntity.setCity(TEST_CITY_NAME);
        return addressEntity;
    }
}


