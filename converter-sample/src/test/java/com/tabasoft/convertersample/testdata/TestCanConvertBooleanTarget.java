package com.tabasoft.convertersample.testdata;

public class TestCanConvertBooleanTarget {
    private boolean test;

    public boolean isTest()
    {
        return test;
    }

    public void setTest(boolean test)
    {
        this.test = test;
    }
}
