package com.tabasoft.converter;

import com.tabasoft.converter.api.ExtendedConversionService;
import com.tabasoft.converter.exception.ConvertibleException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Converter class provides method for converting source <class>S</class> object to target <class>T</class> object.
 * Will be converted only attributes which will have the same getter method in source object
 *
 *  @version "0.0.1 06.06.2018"
 *  @author Andrey Bolut, Yury Andreev
 */
public abstract class Converter<S, T>
{
    private final Logger logger = LoggerFactory.getLogger(Converter.class);

    protected int maxDepth = DEFAULT_DEPTH_VALUE;

    private static final int DEFAULT_DEPTH_VALUE = 5;

    private static final String IS_ACCESSOR_PREFIX = "is";
    private static final String GET_ACCESSOR_PREFIX = "get";

    @Autowired
    private ExtendedConversionService extendedConversionService;

    /**
     * Convert source object to target object with depth = 1
     *
     * @param sourceObject source object
     * @param targetType target object type
     * @return new instance of the converted target object
     * @throws ConvertibleException when target type <class>T</class> cannot be instantiated
     */
    public final T convertFrom(S sourceObject, Class<T> targetType) throws ConvertibleException
    {
        return convertFrom(sourceObject, targetType, 1);
    }

    /**
     * Convert object with target type <class>T</class> to object with source type <class>S</class>
     * The conversion depth = 1
     *
     * @param sourceObject source object
     * @param sourceType target object type
     * @return new instance of the converted target object
     * @throws ConvertibleException when target type <class>S</class> cannot be instantiated
     */
    public final S convertTo(T sourceObject, Class<S> sourceType) throws ConvertibleException
    {
        return convertTo(sourceObject, sourceType, 1);
    }

    /**
     * Convert source object to target object with provided depth
     * If source object has convertible child then this child will be converted also provided that depth > 1
     * Children of child object will be converted to the depth as far as the parameter <code>currentDepth</code> allows
     *
     * @param sourceObject source object
     * @param targetType target object type
     * @param currentDepth parameter for definition of depth of converting
     * @return new instance of the converted target object <class>T</class>
     * @throws ConvertibleException when target type <class>T</class> cannot be instantiated
     */
    public final T convertFrom(S sourceObject, Class<T> targetType, int currentDepth) throws ConvertibleException
    {
        try
        {
            T targetObject = targetType.newInstance();
            convert(sourceObject, targetObject, currentDepth);
            convertFromManual(sourceObject, targetObject);
            return targetObject;
        }
        catch (InstantiationException | IllegalAccessException e)
        {
            throw new ConvertibleException(e, "Target type %s cannot be instantiated", targetType.getName());
        }
    }

    /**
     * Convert object with target type <class>T</class> to object with source type <class>S</class>
     * If source object has convertible child then this child will be converted also provided that depth > 1
     * Children of child object will be converted to the depth as far as the parameter <code>currentDepth</code> allows
     *
     * @param sourceObject source object
     * @param sourceType target object type
     * @param currentDepth parameter for definition of depth of converting
     * @return new instance of the converted target object <class>S</class>
     * @throws ConvertibleException when target type <class>S</class> cannot be instantiated
     */
    public final S convertTo(T sourceObject, Class<S> sourceType, int currentDepth) throws ConvertibleException
    {
        try
        {
            S targetObject = sourceType.newInstance();
            convert(sourceObject, targetObject, currentDepth);
            convertToManual(sourceObject, targetObject);
            return targetObject;
        }
        catch (InstantiationException | IllegalAccessException e)
        {
            throw new ConvertibleException(e, "Target type %s cannot be instantiated", sourceType.getName());
        }
    }

    /**
     * This methods should be override for determine of implementation of manual converting of attributes
     * which can not be converted automatically.
     * Method used for converting from object with source type <class>S</class> to object with target type <class>T</class>
     *
     * @param source source object
     * @param target target object
     */
    protected void convertFromManual(S source, T target)
    {

    }

    /**
     * This methods should be override for determine of implementation of manual converting of attributes
     * which can not be converted automatically.
     * Method used for converting from object with target type <class>T</class> to object with source type <class>S</class>
     *
     * @param source source object
     * @param target target object
     */
    protected void convertToManual(T source, S target)
    {

    }

    private void convert(Object source, Object target, int depth) throws ConvertibleException
    {
        Set<Field> sourceFields = getFieldSetForClass(source.getClass());
        Set<Field> targetFields = getFieldSetForClass(target.getClass());
        Map<String, Field> targetFieldsMap = targetFields.stream().collect(Collectors.toMap(Field::getName, f -> f));
        for (Field sourceField : sourceFields)
        {
            findFieldAndSetValue(source, target, targetFieldsMap, sourceField, depth);
        }
    }

    @SuppressWarnings("unchecked")
    private void findFieldAndSetValue(Object source, Object targetObject, Map<String, Field> targetFieldsMap, Field sourceField, int depth) throws ConvertibleException
    {
        String currentFieldName = sourceField.getName();
        if (Modifier.isFinal(sourceField.getModifiers()))
        {
            return;
        }
        if (targetFieldsMap.containsKey(currentFieldName))
        {
            Field targetField = targetFieldsMap.get(currentFieldName);

            if (sourceField.getType().isAssignableFrom(List.class))
            {
                if (sourceField.getGenericType() instanceof ParameterizedType && targetField.getGenericType() instanceof ParameterizedType)
                {
                    Type parameterizedTypeTarget = ((ParameterizedType) targetField.getGenericType()).getActualTypeArguments()[0];
                    Collection sourceFieldValue = (List) getFieldValue(source, sourceField);
                    Object result = extendedConversionService.convertMany(sourceFieldValue, (Class<?>) parameterizedTypeTarget, depth + 1);
                    setFieldValue(targetObject, targetField, result);
                }
            }
            else
            {
                if (sourceField.getType().equals(targetField.getType()))
                {
                    copyFieldValue(source, targetObject, sourceField, targetField);
                }
                else if (depth < maxDepth)
                {
                    Object value = getFieldValue(source, sourceField);
                    Object result = extendedConversionService.convert(value, targetField.getType(), depth + 1);
                    setFieldValue(targetObject, targetField, result);
                }
            }
        }
    }


    private Set<Field> getFieldSetForClass(Class<?> clazz)
    {
        Set<Field> fields = new HashSet<>();
        Class currentClass = clazz;
        while (!currentClass.equals(Object.class))
        {
            Field[] foundFields = currentClass.getDeclaredFields();
            fields.addAll(Arrays.stream(foundFields).collect(Collectors.toList()));
            currentClass = currentClass.getSuperclass();
        }

        return fields;
    }

    private void copyFieldValue(Object source, Object targetObject, Field sourceField, Field targetField) throws ConvertibleException
    {
        Object value = getFieldValue(source, sourceField);
        setFieldValue(targetObject, targetField, value);
    }

    private void setFieldValue(Object targetObject, Field targetField, Object value)
    {
        if (value != null)
        {
            boolean isTargetFieldAccessible = targetField.isAccessible();
            targetField.setAccessible(true);
            try
            {
                targetField.set(targetObject, value);
            }
            catch (IllegalAccessException e)
            {
                logger.warn(String.format("Value %s cannot be set to the target field %s", value,  targetField.getName()));
            }
            targetField.setAccessible(isTargetFieldAccessible);
        }
    }


    private Object getFieldValue(Object source, Field sourceField) throws ConvertibleException
    {
        Object value = null;
        final String getterName = buildGetterName(sourceField.getName());
        final String isAccessorName = buildIsAccessorNameName(sourceField.getName());
        Method getter = Arrays.stream(source.getClass().getMethods()).filter(method -> method.getName().equals(getterName)
                || method.getName().equals(isAccessorName)).findFirst().orElse(null);
        if (getter != null)
        {
            boolean isGetterAccessible = getter.isAccessible();
            getter.setAccessible(true);
            try
            {
                value = getter.invoke(source);
            }
            catch (IllegalAccessException | InvocationTargetException e)
            {
                logger.warn(String.format("Value cannot be obtained from the getter method  %s", getter.getName()));
            }
            getter.setAccessible(isGetterAccessible);
        }
        else
        {
            boolean isSourceFieldAccessible = sourceField.isAccessible();
            sourceField.setAccessible(true);
            try
            {
                value = sourceField.get(source);
            }
            catch (IllegalAccessException e)
            {
                logger.warn(String.format("Value cannot be obtained from the source filed  %s", sourceField.getName()));
            }
            sourceField.setAccessible(isSourceFieldAccessible);
        }

        return value;
    }

    private String buildGetterName(String fieldName) throws ConvertibleException
    {
        return buildAccessorName(GET_ACCESSOR_PREFIX, fieldName);
    }

    private String buildIsAccessorNameName(String fieldName) throws ConvertibleException
    {
        return buildAccessorName(IS_ACCESSOR_PREFIX, fieldName);
    }

    private String buildAccessorName(String prefix, String fieldName) throws ConvertibleException
    {
        if (fieldName.length() > 0)
        {
            fieldName = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1, fieldName.length());
            return prefix + fieldName;
        }
        throw new ConvertibleException("Filed name has length less than 0");
    }
}
