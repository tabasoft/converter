package com.tabasoft.converter;

/**
 * Default converter class. Will be used if a suitable converter is not found
 *
 *  @version "0.0.1 06.06.2018"
 *  @author Andrey Bolut
 */
public class DefaultConverter extends Converter<Object, Object>
{
}
