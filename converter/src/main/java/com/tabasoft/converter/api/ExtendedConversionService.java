package com.tabasoft.converter.api;

import com.tabasoft.converter.exception.ConvertibleException;
import java.util.Collection;
import javax.annotation.Nullable;
import org.springframework.core.convert.ConversionException;
import org.springframework.core.convert.ConversionService;

/**
 * A service interface for type conversion. This is the entry point into the convert system.
 *
 * @version "0.0.1 06.06.2018"
 * @author Andrey Bolut
 */
public interface ExtendedConversionService extends ConversionService
{
    String BEAN_NAME = "extendedConversionService";

    /**
     * Convert the given collection of {@code source} to the specified collection of {@code targetType}.
     *
     * @param source the source object to convert (may be {@code null})
     * @param targetType the target type to convert to (required)
     * @return the converted collection of objects, an instance of targetType
     * @throws ConversionException if a conversion exception occurred
     * @throws IllegalArgumentException if targetType is {@code null}
     */
    <T> Collection<T> convertMany(Collection<Object> source, Class<T> targetType) throws ConvertibleException;

    /**
     * Convert the given collection of {@code source} to the specified collection of {@code targetType}.
     * The conversion will extend in depth, based on the provided parameter {@code currentDepth}
     *
     * @param source the source object to convert (may be {@code null})
     * @param targetType the target type to convert to (required)
     * @param currentDepth parameter for definition of depth of converting
     * @return the converted collection of objects, an instance of targetType
     * @throws ConversionException if a conversion exception occurred
     * @throws IllegalArgumentException if targetType is {@code null}
     */
    <T> Collection<T> convertMany(Collection<Object> source, Class<T> targetType, int currentDepth);

    /**
     * Convert the given {@code source} to the specified {@code targetType}.
     *
     * @param source the source object to convert (may be {@code null})
     * @param targetType the target type to convert to (required)
     * @param currentDepth parameter for definition of depth of converting
     * @return the converted object, an instance of targetType
     * @throws ConversionException if a conversion exception occurred
     * @throws IllegalArgumentException if targetType is {@code null}
     */
    <T> T convert(@Nullable Object source, Class<T> targetType, int currentDepth) throws ConvertibleException;
}
