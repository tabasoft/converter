package com.tabasoft.converter.api.impl;

import com.tabasoft.converter.Converter;
import com.tabasoft.converter.DefaultConverter;
import com.tabasoft.converter.api.ExtendedConversionService;
import com.tabasoft.converter.exception.ConvertibleException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

/**
 * A implementation of {@link ExtendedConversionService}
 *
 * @version "0.0.1 06.06.2018"
 * @author Andrey Bolut
 */
@Service(ExtendedConversionService.BEAN_NAME)
public class ExtendedConversionServiceImpl implements ExtendedConversionService
{
    private final Logger logger = LoggerFactory.getLogger(ExtendedConversionServiceImpl.class);

    private Collection<Converter> converters;
    private Map<Class, Map<Class, Converter>> converterCache;
    @Autowired
    private DefaultConverter defaultConverterInstance;
    @Autowired
    private ApplicationContext context;

    public ExtendedConversionServiceImpl()
    {
        converterCache = new HashMap<>();
    }

    @PostConstruct
    public void postConstruct()
    {
        findConverters();
    }

    /**
     * {@inheritDoc}
     */
    public <T> Collection<T> convertMany(Collection<Object> source, Class<T> targetType) throws ConvertibleException
    {

        return convertMany(source, targetType, 1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> Collection<T> convertMany(Collection<Object> source, Class<T> targetType, int currentDepth)
    {
        return source.stream().map(element -> {
            try
            {
                return convert(element, targetType, currentDepth);
            }
            catch (ConvertibleException e)
            {
                logger.error("Converting was failed.", e);
            }
            return null;
        }).collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    public boolean canConvert(@Nullable Class<?> sourceType, @Nonnull Class<?> targetType)
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public boolean canConvert(@Nullable TypeDescriptor sourceType, @Nonnull TypeDescriptor targetType)
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Nullable
    public <T> T convert(@Nullable Object source, @Nonnull Class<T> targetType)
    {
        try
        {
            return convert(source, targetType, 1);
        }
        catch (ConvertibleException e)
        {
            logger.error("Converting was failed.", e);
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Nullable
    @SuppressWarnings("unchecked")
    public <T> T convert(@Nullable Object source, Class<T> targetType, int currentDepth) throws ConvertibleException
    {
        if (source == null)
        {
            return null;
        }
        Class sourceType = source.getClass();
        Converter converter = getConverterForTypes(sourceType, targetType);
        if (converter.getClass().getGenericSuperclass() instanceof ParameterizedType)
        {
            ParameterizedType converterParametrizedType = (ParameterizedType) converter.getClass().getGenericSuperclass();
            Type[] actualTypeArguments = converterParametrizedType.getActualTypeArguments();
            if (actualTypeArguments.length != 2)
            {
                throw new ConvertibleException("The length of actula type arguments should be equals '2'");
            }
            Type firstGenericClass = actualTypeArguments[0];
            Type secondGenericClass = actualTypeArguments[1];
            if (firstGenericClass.getTypeName().equals(sourceType.getName()) &&
                    secondGenericClass.getTypeName().equals(targetType.getName()))
            {
                return (T) converter.convertFrom(source, targetType, currentDepth);
            }
            else
            {
                return (T) converter.convertTo(source, targetType, currentDepth);
            }
        }
        else
        {
            return (T) converter.convertFrom(source, targetType, currentDepth);
        }
    }

    /**
     * {@inheritDoc}
     */
    public Object convert(Object source, TypeDescriptor sourceType, @Nonnull TypeDescriptor targetType)
    {
        return null;
    }

    public Collection<Converter> getConverters()
    {
        return converters;
    }

    private Converter getConverterForTypes(Class sourceType, Class targetType)
    {
        if (converterCache.containsKey(sourceType))
        {
            Map<Class, Converter> sourceTypeConverters = converterCache.get(sourceType);
            if (sourceTypeConverters.containsKey(targetType))
            {
                return sourceTypeConverters.get(targetType);
            }
        }
        for (Converter converter : converters)
        {
            Type converterType = converter.getClass().getGenericSuperclass();
            if (converterType instanceof ParameterizedType)
            {
                ParameterizedType converterParametrizedType = (ParameterizedType) converterType;
                Type[] actualTypeArguments = converterParametrizedType.getActualTypeArguments();
                if (actualTypeArguments.length != 2)
                {
                    continue;
                }
                if (isSuitableConverterForType(sourceType, targetType, actualTypeArguments))
                {
                    if (!converterCache.containsKey(sourceType))
                    {
                        converterCache.put(sourceType, new HashMap<>());
                    }
                    Map<Class, Converter> sourceTypeConverterMap = converterCache.get(sourceType);
                    sourceTypeConverterMap.put(targetType, converter);
                    return converter;
                }
            }
        }

        return defaultConverterInstance;
    }

    private boolean isSuitableConverterForType(Class sourceType, Class targetType, Type[] actualTypeArguments)
    {
        Type firstGenericClass = actualTypeArguments[0];
        Type secondGenericClass = actualTypeArguments[1];
        return (firstGenericClass.getTypeName().equals(sourceType.getName()) &&
                secondGenericClass.getTypeName().equals(targetType.getName())) ||
                (firstGenericClass.getTypeName().equals(targetType.getName()) &&
                        secondGenericClass.getTypeName().equals(sourceType.getName()));
    }

    private void findConverters()
    {
        Map<String, Converter> converters = context.getBeansOfType(Converter.class);
        this.converters = new ArrayList<>(converters.values());
    }
}
