package com.tabasoft.converter.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.tabasoft.converter")
public class ExtendedConversionServiceConfig
{
}
