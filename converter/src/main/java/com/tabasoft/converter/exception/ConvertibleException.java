package com.tabasoft.converter.exception;

/**
 * The class {@code ConvertibleException} is a form of {@code Throwable} that indicates conditions that a reasonable
 * application might want to catch.
 *
 *  @version "0.0.1 06.06.2018"
 *  @author Yury Andreev
 */
public class ConvertibleException extends Exception
{
    /**
     * Constructs a new exception with the specified detail message
     *
     * @param message the detail message
     */
    public ConvertibleException(String message)
    {
        super(message);
    }

    /**
     * Constructs a new exception with the specified detail message
     *
     * @param message the detail message
     * @param params the values which should be placed to the message
     */
    public ConvertibleException(String message, Object... params)
    {
        this(String.format(message, params));
    }

    /**
     * Constructs a new exception with the specified detail message
     *
     * @param e the cause
     * @param message the detail message
     * @param params the values which should be placed to the message
     */
    public ConvertibleException(Exception e, String message, Object... params)
    {
        super(String.format(message, params), e);
    }
}
