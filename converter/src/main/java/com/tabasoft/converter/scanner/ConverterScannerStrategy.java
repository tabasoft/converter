package com.tabasoft.converter.scanner;

import com.tabasoft.converter.Converter;
import java.util.Collection;

/**
 * Interface provides methods for possibility to find all converter classes
 *
 * @version "0.0.1 06.06.2018"
 * @author Andrey Bolut
 */
public interface ConverterScannerStrategy
{
    /**
     * Returns collection of converters
     *
     * @param converterLocations package where converters will be searched
     * @return collection of instances of classes which extends {@link Converter} class
     */
    Collection<Class<? extends Converter>> findConverters(String converterLocations);
}
