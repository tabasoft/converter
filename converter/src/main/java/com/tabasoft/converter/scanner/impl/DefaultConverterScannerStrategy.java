package com.tabasoft.converter.scanner.impl;

import com.tabasoft.converter.Converter;
import com.tabasoft.converter.scanner.ConverterScannerStrategy;
import java.util.Collection;
import org.reflections.Reflections;
import org.springframework.stereotype.Service;

/**
 * Provide default implementation for searching converter classes
 *
 * @version "0.0.1 06.06.2018"
 * @author Andrey Bolut
 */
@Service("converterScannerStrategy")
public class DefaultConverterScannerStrategy implements ConverterScannerStrategy
{
    /**
     * {@inheritDoc}
     */
    public Collection<Class<? extends Converter>> findConverters(String converterLocations)
    {
        Reflections reflections = new Reflections(converterLocations);
        return reflections.getSubTypesOf(Converter.class);
    }
}
