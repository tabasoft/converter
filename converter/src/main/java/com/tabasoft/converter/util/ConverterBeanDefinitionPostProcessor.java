package com.tabasoft.converter.util;

import com.tabasoft.converter.Converter;
import com.tabasoft.converter.scanner.impl.DefaultConverterScannerStrategy;
import java.util.Collection;
import javax.annotation.Nonnull;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.annotation.Configuration;

/**
 * Provides possibility to register all converters as Spring bean.
 *
 * @version "0.0.1 06.06.2018"
 * @author Andrey Bolut
 */
@Configuration
public class ConverterBeanDefinitionPostProcessor implements BeanDefinitionRegistryPostProcessor
{
    @Value("${converter.locations}")
    private String converterLocations;

    /**
     * Register all converter classes which extends {@link Converter} as Spring bean. This allows for adding further
     * bean definitions before the next post-processing phase kicks in
     *
     * @param beanDefinitionRegistry instance of {@link BeanDefinitionRegistry} for registries that hold bean definitions
     * @throws BeansException if registering of bean definition was failed
     */
    public void postProcessBeanDefinitionRegistry(@Nonnull BeanDefinitionRegistry beanDefinitionRegistry) throws BeansException
    {
        DefaultConverterScannerStrategy defaultConverterScannerStrategy = new DefaultConverterScannerStrategy();

        Collection<Class<? extends Converter>> classes = defaultConverterScannerStrategy.findConverters(converterLocations);
        classes.forEach(clazz -> {
            BeanDefinitionBuilder bdf = BeanDefinitionBuilder.genericBeanDefinition(clazz);
            beanDefinitionRegistry.registerBeanDefinition(clazz.getCanonicalName(), bdf.getBeanDefinition());
        });

    }

    /**
     * {@inheritDoc}
     */
    public void postProcessBeanFactory(@Nonnull ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException
    {
        //ignored
    }
}

